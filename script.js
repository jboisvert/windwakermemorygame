var memoryGame = {};

//Default value is false since page has not fully loaded
memoryGame.loaded = false;
memoryGame.choices = [];
memoryGame.cardsFlipped = [];
memoryGame.secretImages = [];
memoryGame.secretIndex = 0;

//Creating card class 
function Card(cardElement, faceElement) {

    this.cardElement = cardElement;
    this.faceElement = faceElement;

}

//Called when the page has loaded
function init() {

    if (memoryGame.loaded == true) {

        cacheSecretImages();
        addListenersToMemoryGame();
        setUpNavButtons();
        shuffleCards();

    }

}

function cacheSecretImages() {

    secret1 = new Image();
    //Source of image : https://assets.vg247.com/current//2013/08/91361_WUPP_BCZ_z_sakanaotoko_ad.jpg
    secret1.src = "images/cards/secretImage.jpg";
    memoryGame.secretImages.push(secret1);

    secret2 = new Image();
    //Source of image : https://i.neoseeker.com/ca/the_legend_of_zelda_the_wind_waker_hd_conceptart_C7E95.jpg
    secret2.src = "images/cards/secretImageDragon.jpg";
    memoryGame.secretImages.push(secret2);

    secret3 = new Image();
    //Source of image : http://www.glitterberri.com/images/graphics/tww/link_pigs.jpg
    secret3.src = "images/cards/secretImagePigs.jpg";
    memoryGame.secretImages.push(secret3);

    secret4 = new Image();
    //Source of image : https://cdn1.spong.com/artwork/l/e/legendofze401681l/_-Legend-Of-Zelda-The-Wind-Waker-GameCube-_.jpg
    secret4.src = "images/cards/secretImageToads.jpg";
    memoryGame.secretImages.push(secret4);

    secret5 = new Image();
    //Source of image : https://www.zeldaeurope.de/galerie/albums/userpics/10003/normal_wiiu_tloz-wind-waker-hd_artwork_023.jpg
    secret5.src = "images/cards/secretImageSeagulls.jpg";
    memoryGame.secretImages.push(secret5);

}

function setUpNavButtons() {


    if (document.getElementById("start") && document.getElementById("start").addEventListener) {


        document.getElementById("start").addEventListener("click", replayButton, false);
    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.getElementById("start") && document.getElementById("start").attachEvent) {

        document.getElementById("start").attachEvent("on" + "click", replayButton);

    }

    //End now button
    if (document.getElementById("endNow") && document.getElementById("endNow").addEventListener) {


        document.getElementById("endNow").addEventListener("click", endButton, false);
    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.getElementById("endNow") && document.getElementById("endNow").attachEvent) {

        document.getElementById("endNow").attachEvent("on" + "click", endButton);

    }

    //Close help button
    if (document.getElementById("closeHelp") && document.getElementById("closeHelp").addEventListener) {


        document.getElementById("closeHelp").addEventListener("click", closeHelp, false);
    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.getElementById("closeHelp") && document.getElementById("closeHelp").attachEvent) {

        document.getElementById("closeHelp").attachEvent("on" + "click", closeHelp);

    }

}

//used to make the how to play box be no longer visible
function closeHelp() {

    document.getElementById("howToPlay").style.display = "none";
}

//This method is used to make all the css properties of the cards go back to normal.
function replayButton() {

    //Make sure the array is empty 
    memoryGame.cardsFlipped = [];

    switchSecretImage();

    var cards = document.getElementsByClassName("card");

    addListenersToMemoryGame();

    //<ake all the cards reappear and reset default settings
    for (var i = 0; i < cards.length; i++) {

        cards[i].style.opacity = 1;
        var faceOfImage = getFaceFromCard(cards[i]);
        faceOfImage.style.zIndex = 1;

    }

    shuffleCards();

}

//Used to switch the secret image on the board when restarting a game
//And keeps track of which image to change to 
function switchSecretImage() {

    memoryGame.secretIndex++;

    //If the index is causing an array out of bounds cycle back to the beginning
    if (memoryGame.secretIndex >= memoryGame.secretImages.length) {

        memoryGame.secretIndex = 0;

    }

    document.getElementById("memoryGame").style.backgroundImage = "url(" + memoryGame.secretImages[memoryGame.secretIndex].src + ")";

}

//Function that makes all the cards see through 
function endButton() {

    var cards = document.getElementsByClassName("card");

    removeListenersToMemoryGame();

    //Make all the 'cards' no longer visible. 
    for (var i = 0; i < cards.length; i++) {

        cards[i].style.opacity = 0;


    }

}

//Shuffles all the images in the memory game
function shuffleCards() {

    var cards = document.getElementsByClassName("card");

    for (var i = 0; i < cards.length; i++) {

        var indexeCardImage = getFaceFromCard(cards[i]);
        //Get a random index between 0 and the length of the cards collection minus 1
        var randomIndex = Math.floor(Math.random() * cards.length);
        //Get the reference of the image of a random card
        var randomCardImage = getFaceFromCard(cards[randomIndex]);

        //Swap the images
        var temp = indexeCardImage.src;
        indexeCardImage.src = randomCardImage.src;
        randomCardImage.src = temp;

    }

}

//Returns a reference to the image (of the face) in the card
function getFaceFromCard(card) {

    if (card.className == "card") {

        var childNodesOfSelection = card.childNodes;

        for (var i = 0; i < childNodesOfSelection.length; i++) {

            var classNameOfNode = childNodesOfSelection[i].className;
            if (classNameOfNode == "face") {

                return childNodesOfSelection[i];

            }

        }

    }

}

//For when the card is clicked
function cardSelectionClick(event) {

    var className = event
        .target.parentNode.className;

    var cardElement = event
        .target.parentNode;

    if (className == "card") {

        cardSelection(cardElement);

    }

}

//The actual memory game logic is done here where it checks if the user has made two choices
function cardSelection(card) {

    //Checks if the event is a card and ensures it has not already been selected. 
    if (card.className == "card" && alreadySelected(card) == false && isCardAlreadyFlipped(card) == false) {

        var childNodesOfSelection = card.childNodes;

        for (var i = 0; i < childNodesOfSelection.length; i++) {

            var classNameOfNode = childNodesOfSelection[i].className;
            if (classNameOfNode == "face") {

                childNodesOfSelection[i].style.zIndex = 2;

                memoryGame.choices.push(new Card(card, childNodesOfSelection[i]));

                break;
            }

        }

        //If two choices were made, now compare them 
        if (memoryGame.choices.length == 2) {


            //Makes it so user cannot click on any more cards while checking the two choices made
            removeListenersToMemoryGame();

            //Waits 2 seconds before checking if the two cards selected are the same
            setTimeout(checkChoices, 1000);

        }

    }

}

//Checks if the reference of the card is already being referenced to (pointer) in the memoryGame.choices array
function alreadySelected(card) {

    //Checks if it is a card if not then false
    if (card.className == "card") {

        for (var i = 0; i < memoryGame.choices.length; i++) {

            //If the current card in question is the same reference to the card made in the first choice means 
            //this card was already chosen
            if (memoryGame.choices[i].cardElement == card) {

                return true;

            }

        }

        //Can only get here if card was never found in loop
        return false;

    }

    return false;

}

//Checks if the given card is part of the cardsFlipped array to ensure an already selected card cannot be selected again.
function isCardAlreadyFlipped(card) {

    //Checks if it is a card if not then false
    if (card.className == "card") {

        for (var i = 0; i < memoryGame.cardsFlipped.length; i++) {

            //If the current card in question is the same reference to the card made in the first choice means 
            //this card was already chosen
            if (memoryGame.cardsFlipped[i] == card) {

                return true;

            }

        }

        //Can only get here if card was never found in loop
        return false;

    }

    return false;

}

//Sets up the on click event listener for the memory game
function addListenersToMemoryGame() {

    //Adding the keyboard       
    if (document.body && document.body.addEventListener) {

        document.body.addEventListener("keyup", keyCardSelection, false);

    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.body && document.body.attachEvent) {

        document.body.attachEvent("on" + "keyup", keyCardSelection);

    }

    //Adding click listener to memory game    
    if (document.getElementById("memoryGame") && document.getElementById("memoryGame").addEventListener) {

        document.getElementById("memoryGame").addEventListener("click", cardSelectionClick, false);

    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.getElementById("memoryGame") && document.getElementById("memoryGame").attachEvent) {

        document.body.attachEvent("on" + "click", cardSelectionClick);

    }

}

function removeListenersToMemoryGame() {

    //Removing the keyboard       
    if (document.body && document.body.removeEventListener) {

        document.body.removeEventListener("keyup", keyCardSelection);

    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.body && document.body.detachEvent) {

        document.body.detachEvent("onkeyup", keyCardSelection);

    }

    //Removing click listener to memory game    
    if (document.getElementById("memoryGame") && document.getElementById("memoryGame").removeEventListener) {

        document.getElementById("memoryGame").removeEventListener("click", cardSelectionClick, false);

    }
    //Done for older version of Opera (6 and lower) and IE 8 and lower
    else if (document.getElementById("memoryGame") && document.getElementById("memoryGame").detachEvent) {

        document.body.detachEvent("onclick", cardSelectionClick);

    }

}

function keyCardSelection(event) {

    //To stop the user from hitting other keys (to select cards) when two
    //choices were already made. 
    if (memoryGame.choices.length < 2) {

        var capital = "A";
        var lowerCase = "a";

        var keyPressed = event.key;

        var cards = document.getElementsByClassName("card");

        for (var i = 0; i < cards.length; i++) {

            if (keyPressed == capital || keyPressed == lowerCase) {

                cardSelection(cards[i]);

                //No need to continue looping since key was found
                break;

            }

            //Get the char code of the lowerCase and capital and go to the next character
            var charCode = capital.charCodeAt(0) + 1;
            capital = String.fromCharCode(charCode);

            charCode = lowerCase.charCodeAt(0) + 1;
            lowerCase = String.fromCharCode(charCode);

        }


    }




}

//Used to check if the two cards selected are the same or not
function checkChoices() {

    var imageSourceOne = memoryGame.choices[0].faceElement.src;
    var imageSourceTwo = memoryGame.choices[1].faceElement.src;

    //Checking if the urls equal one another
    var areTheyEqual = imageSourceOne.localeCompare(imageSourceTwo);

    //They are equal so do not display them anymore
    if (areTheyEqual == 0) {

        memoryGame.choices[0].cardElement.style.opacity = "0";

        //Stores the 'card' reference in an array to know when the game is over and to not allow card to be selected again 
        memoryGame.cardsFlipped.push(memoryGame.choices[0].cardElement)

        memoryGame.choices[1].cardElement.style.opacity = "0";

        //Stores the 'card' reference in an array to know when the game is over and to not allow card to be selected again 
        memoryGame.cardsFlipped.push(memoryGame.choices[1].cardElement)

        //Make the cards clickable again
        addListenersToMemoryGame();


    }
    //They do not have the same image so re-show them
    else {

        memoryGame.choices[0].faceElement.style.zIndex = 1;
        memoryGame.choices[1].faceElement.style.zIndex = 1;

        //Make the cards clickable again 
        addListenersToMemoryGame();

    }

    //Empty the array
    memoryGame.choices = [];

    //If the length of the array is 16 means the user has won, let's congradulate them
    if (memoryGame.cardsFlipped.length == 16) {

        alert("Congrats you won! Play Again to see more secret images!");

    }


}


//Waiting for document to be fully loaded    
if (document && document.addEventListener) {

    //When the dom is loaded this will be called
    document.addEventListener("DOMContentLoaded", function(event) {

        memoryGame.loaded = true;
        init();

    });
}
//Done for older version of Opera (6 and lower) and IE 8 and lower
else if (document && document.attachEvent) {

    document.attachEvent("on" + "DOMContentLoaded", function(event) {

        memoryGame.loaded = true;
        init();

    });
}